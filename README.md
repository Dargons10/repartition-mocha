Scripts to repartition mocha, changing the system partition from 1.2GB to 3GB to be able to install A10 onwards.


Steps to follow: NOTE(In principle it only works from linux).

A.- From repository:
====================
1.- git clone https://gitlab.com/Dargons10/repartition-mocha.git repartition/
2.- cd repartition/
3.- Enter recovery mode (POWER and VOL +)
4.- Run in terminal bash repartition.sh
5.- It will copy to the tablet the necessary files and will enter directly into the tablet shell.
6.- Choose 16 or 64 depending on your tablet's GB size. Starts repartition.
7 .- At the end reboot the tablet in recovery.

TODO:  Format system in recovery

B.- From ZIP:
=============
1.- Unzip file in repartition/ folder.
2.- cd repartition/
3.- Enter tablet in recovery mode (POWER and VOL +)
4.- Execute in terminal bash repartition.sh
5.- It will copy the necessary files to the tablet and will enter directly into the tablet shell.
6.- Choose 16 or 64 depending on your tablet's GB size. Starts repartition.
7 .- At the end reboot the tablet in recovery.

TODO: Format system in recovery


NOTE.- Before performing the process you should be aware of the risks that may be involved such as getting to hardbrick. If you still want to carry out I am not responsible for any damage that may occur to the tablet. 

