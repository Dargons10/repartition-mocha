#!/bin/bash

adb push parted /sbin/parted
adb push scripts/repartition16.sh /sbin/repartition16.sh
adb push scripts/repartition64.sh /sbin/repartition64.sh

# Function to handle repartitioning based on choice
repartition() {
	case $1 in
	16)
		adb shell sh /sbin/repartition16.sh
		echo "Repartitioning for 16GB completed (if successful)."
		;;
	64)
		adb shell sh /sbin/repartition64.sh
		echo "Repartitioning for 64GB completed (if successful)."
		;;
	*)
		echo "Invalid option. Please choose 1 or 2."
		exit 1
		;;
	esac
}

# Define options
OPTION_16="16"
OPTION_64="64"

# Clearer menu display
echo "Choose an option:"
echo "1) $OPTION_16 GB"
echo "2) $OPTION_64 GB"

# Read user's choice
read -p "Your choice: " choice

# Validate and handle choice
repartition $choice
