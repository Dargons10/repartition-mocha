#!/sbin/sh
#Le damos permisos a parted
chmod 777 /sbin/parted

#Listamos las particiones
parted /dev/block/mmcblk0 p free

#Desmontamos las particiones que nos interesan
umount -l /dev/block/mmcblk0p29
umount -l /dev/block/mmcblk0p28
umount -l /dev/block/mmcblk0p27
umount -l /dev/block/mmcblk0p26

#Borramos las particiones desde la 29 hasta la 26
parted /dev/block/mmcblk0 rm 29
parted /dev/block/mmcblk0 rm 28
parted /dev/block/mmcblk0 rm 27
parted /dev/block/mmcblk0 rm 26

#Creamos las particiones nuevamente con el tamaño nuevo.
parted /dev/block/mmcblk0 mkpart primary ext4 268MB 3340MB 
parted /dev/block/mmcblk0 mkpart primary ext4 3340MB 3345MB
parted /dev/block/mmcblk0 mkpart primary ext4 3345MB 3747MB
parted /dev/block/mmcblk0 mkpart primary ext4 3747MB 15.8GB

#Nombramos correctamente las particiones
parted /dev/block/mmcblk0 name 26 APP
parted /dev/block/mmcblk0 name 27 AP1
parted /dev/block/mmcblk0 name 28 CAC
parted /dev/block/mmcblk0 name 29 UDA

#Activamos que se puedan formetear
parted /dev/block/mmcblk0 set 29 msftdata on
parted /dev/block/mmcblk0 set 28 msftdata on
parted /dev/block/mmcblk0 set 27 msftdata on
parted /dev/block/mmcblk0 set 26 msftdata on

parted /dev/block/mmcblk0 p free

#Formateamos las particiones.
mke2fs -b 4096 -T ext4 /dev/block/mmcblk0p29
mke2fs -b 4096 -T ext4 /dev/block/mmcblk0p28
mke2fs -b 4096 -T ext4 /dev/block/mmcblk0p27
mke2fs -b 4096 -T ext4 /dev/block/mmcblk0p26

#Comprobamos que están las particiones 26,27,28,29
parted /dev/block/mmcblk0 p free
